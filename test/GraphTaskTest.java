import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

   @Test (timeout=20000)
   public void test1() {
      GraphTask.Graph g = new GraphTask().new Graph("aa");
      g.createRandomSimpleGraph(6, 9);
      GraphTask.Graph gclone = (GraphTask.Graph) g.clone();
      assertEquals (g, gclone);
   }

   @Test (timeout=20000)
   public void test2() {
      GraphTask.Graph g = new GraphTask().new Graph("manual");
      g.createRandomSimpleGraph(15, 25);
      GraphTask.Graph gclone = (GraphTask.Graph) g.clone();
      assertEquals (g, gclone);
   }

   @Test (timeout=20000)
   public void testAdd() {
      GraphTask.Graph g = new GraphTask().new Graph("manual");
      g.createRandomSimpleGraph(6, 8);
      GraphTask.Graph gclone = (GraphTask.Graph) g.clone();
      g.getFirst().setId("deepcloneproof");
      assertEquals (g, gclone);
   }


   @Test (timeout=20000)
   public void testGraphEmpty() {
      GraphTask.Graph g = new GraphTask().new Graph("aa");
      GraphTask.Graph gclone = (GraphTask.Graph) g.clone();
      assertEquals (g, gclone);
   }

}

