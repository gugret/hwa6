import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 * @since 1.8
 */
public class GraphTask {

   /**
    * Main method.
    */
   public static void main(String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /**
    * Actual main method to run examples and everything.
    * Additional tests at https://bitbucket.org/gugret/hwa6/src/master/test/GraphTaskTest.java
    */
   public void run() {
      Graph g = new Graph("G");
      g.createRandomSimpleGraph(6, 7);
      Graph gclone = (Graph) g.clone();
      System.out.println(g);
      System.out.println(gclone);
      Graph bigGraph = new Graph("Biiig");
      bigGraph.createRandomSimpleGraph(2000, 1999);
      long startTime = System.nanoTime();
      Graph bigGraphclone = (Graph) bigGraph.clone(); //This counts as one of five tests, right?
      long endTime = System.nanoTime();
      System.out.println((endTime - startTime) + " nanoseconds");
      System.out.println(g.getFirst().getFirst());
      System.out.println(g.getFirst().getNext());
      System.out.println(g.getFirst().getNext().getNext());
      System.out.println(g.getFirst().getNext().getNext().getNext());
      System.out.println(g.getFirst().getNext().getNext().getNext().getNext());
      System.out.println(g.getFirst().getFirst().getTarget().getNext());
      System.out.println(g.getFirst().getNext().getNext());
      System.out.println(g.getFirst().getFirst());
      System.out.println(g.getFirst().getFirst().getTarget());
      System.out.println(g.getFirst().getFirst().getNext());
   }

   /**
    * A vertex represents a node in the graph.
    *
    * Problem 14. Construct deep clone of a given graph.
    * Deep clones can be tricky, as opposed to a shallow clone.
    * A shallow clone of a java object only copies its values, rendering it dependent on the original.
    * A deep clone creates a fully independent, exact replica of the object,
    * therefore a deep clone will not be affected by future changes to the original.
    */

   class Vertex {

      private String id;

      public String getId() {
         return id;
      }

      public void setId(String id) {
         this.id = id;
      }

      public Vertex getNext() {
         return next;
      }

      public void setNext(Vertex next) {
         this.next = next;
      }

      public Arc getFirst() {
         return first;
      }

      public void setFirst(Arc first) {
         this.first = first;
      }

      public int getInfo() {
         return info;
      }

      public void setInfo(int info) {
         this.info = info;
      }

      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex(String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      @Override
      public Vertex clone() {
         return new Vertex(getId(), getNext(), getFirst());
      }

   }


   /**
    * Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      public String getId() {
         return id;
      }

      public void setId(String id) {
         this.id = id;
      }

      public Vertex getTarget() {
         return target;
      }

      public void setTarget(Vertex target) {
         this.target = target;
      }

      public Arc getNext() {
         return next;
      }

      public void setNext(Arc next) {
         this.next = next;
      }

      public int getInfo() {
         return info;
      }

      public void setInfo(int info) {
         this.info = info;
      }

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc(String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!
      @Override
      public Arc clone() {
         return new Arc(getId(), getTarget(), getNext());
      }
   }


   /**
    * This header represents a graph.
    */
   class Graph {

      public String getId() {
         return id;
      }

      public void setId(String id) {
         this.id = id;
      }

      public Vertex getFirst() {
         return first;
      }

      public void setFirst(Vertex first) {
         this.first = first;
      }

      public int getInfo() {
         return info;
      }

      public void setInfo(int info) {
         this.info = info;
      }

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph(String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph(String s) {
         this(s, null);
      }

      Graph() {
         this(null, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty("line.separator");
         StringBuffer sb = new StringBuffer(nl);
         sb.append(id);
         sb.append(nl);
         Vertex v = first;
         while (v != null) {
            sb.append(v.toString());
            sb.append(" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append(" ");
               sb.append(a.toString());
               sb.append(" (");
               sb.append(v.toString());
               sb.append("->");
               sb.append(a.target.toString());
               sb.append(")");
               a = a.next;
            }
            sb.append(nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex(String vid) {
         Vertex res = new Vertex(vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc(String aid, Vertex from, Vertex to) {
         Arc res = new Arc(aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       *
       * @param n number of vertices added to this graph
       */
      public void createRandomTree(int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex[n];
         for (int i = 0; i < n; i++) {
            varray[i] = createVertex("v" + String.valueOf(n - i));
            if (i > 0) {
               int vnr = (int) (Math.random() * i);
               createArc("a" + varray[vnr].toString() + "_"
                       + varray[i].toString(), varray[vnr], varray[i]);
               createArc("a" + varray[i].toString() + "_"
                       + varray[vnr].toString(), varray[i], varray[vnr]);
            } else {
            }
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       *
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int[info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res[i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       *
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph(int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException("Too many vertices: " + n);
         if (m < n - 1 || m > n * (n - 1) / 2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + m);
         first = null;
         createRandomTree(n);       // n-1 edges created here
         Vertex[] vert = new Vertex[n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int) (Math.random() * n);  // random source
            int j = (int) (Math.random() * n);  // random target
            if (i == j)
               continue;  // no loops
            if (connected[i][j] != 0 || connected[j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert[i];
            Vertex vj = vert[j];
            createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected[i][j] = 1;
            createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected[j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }


      /**
       * Deep clones the current graph.
       * Overrides java's inbuilt super object clone method.
       * Returns an exact, fully independent copy of the graph.
       */
      @Override
      public Object clone() {
         Graph copy = new Graph(getId());
         if (first == null) {
            return copy;
         }

         Vertex v = first; //Can't put the first round into a loop
         Vertex vcopy = v.clone();
         copy.first = vcopy;
         Map<String, Vertex> vcopies = new HashMap<>();
         vcopies.put(vcopy.id, vcopy);
         v = v.next;

         while (v != null) { //Same as what's done at start, just as a loop
            vcopy.next = v.clone();
            v = v.next;
            vcopy = vcopy.next;
            vcopies.put(vcopy.id, vcopy);
         }

         vcopy = copy.first; //Reset to the actual first
         v = first;
         while (v != null) {
            Arc arc = v.first;
            if (arc == null) {
               v = v.next;
               continue; //breaks the rest of the loop
            }
            Arc acopy = arc.clone();
            acopy.target = vcopies.get(arc.target.id);
            vcopy.first = acopy;
            arc = arc.next;

            while (arc != null) { //The same loop as all those before
               acopy.next = arc.clone();
               acopy.next.target = vcopies.get(arc.target.id);
               acopy = acopy.next;
               arc = arc.next;
            }
            vcopy = vcopy.next;
            v = v.next;
         }
         return copy;
      }
   }
}